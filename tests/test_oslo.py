from typer.testing import CliRunner

from main import app

runner = CliRunner()


def test_lifecycle_configuration_enabled():
    result = runner.invoke(app, "fedora-testing-farm-image-import")
    assert "Expire objects after 7 days" in result.stdout
    assert "Enabled" in result.stdout